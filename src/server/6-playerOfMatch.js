const matches = require('./../public/output/matches.json')
const deliveriesData = require('./../public/output/deliveries.json')
const fs = require('fs')
const { log } = require('console')

//6. Find a player who has won the highest number of Player of the Match awards for each season
try {
  const matchAwardsEachSeason = () => {
    let playerCounts = {}

    const playY = matches.forEach(element => {
      const season = element.season
      const player_of_match = element.player_of_match
      if (!playerCounts[season]) {
        playerCounts[season] = {}
      }
      if (playerCounts[season]) {
        if (playerCounts[season][player_of_match]) {
          playerCounts[season][player_of_match] += 1
        } else {
          playerCounts[season][player_of_match] = 1
        }
      }
    })

    const playerOfTheMatchCounts = playerCounts

    const highestPlayersPerSeason = {}

    Object.keys(playerOfTheMatchCounts).forEach(season => {
      const playersCount = playerOfTheMatchCounts[season]
      // console.log(playersCount)

      const highestPlayer = Object.keys(playersCount).reduce((a, b) =>
        playersCount[a] > playersCount[b] ? a : b
      )
      // console.log(highestPlayer)
      highestPlayersPerSeason[season] = {
        player: highestPlayer,
        count: playersCount[highestPlayer]
      }
    })

    return highestPlayersPerSeason
  }

  const res = matchAwardsEachSeason(matches)

  fs.writeFileSync(
    `./../public/output/PlayerOfTheMatches.json`,
    JSON.stringify(res, null, 2)
  )
  
} catch (error) {
  console.log(error)
}
