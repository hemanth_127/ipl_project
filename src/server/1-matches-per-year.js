const matchesData = require('./../public/output/matches.json')
const fs = require('fs')

// 1.Number of matches played per year for all the years in IPL.
const matchPerYear = () => {
  try {
    const matches = matchesData.reduce((acc, element) => {
      if (element.season) {
        if (element.season in acc) {
          acc[element.season] += 1
        } else {
          acc[element.season] = 1
        }
      }
      return acc
    }, {})
    return matches
  } catch (err) {
    throw new Error(err)
  }
}

try {
  const res = matchPerYear(matchesData)

  fs.writeFileSync(
    `./../public/output/matchesWonPerYear.json`,
    JSON.stringify(res, null, 2)
  )
} catch (error) {
  console.log(error)
}
