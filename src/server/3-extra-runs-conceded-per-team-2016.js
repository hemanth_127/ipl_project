const matchesData = require('./../public/output/matches.json')
const deliveriesData = require('./../public/output/deliveries.json')
const fs = require('fs')

// 3 . Extra runs conceded per team in the year 2016

try {
  const calculateExtraRunsConceded = (
    deliveriesData,
    matchesData,
    targetYear
  ) => {
    const matchesInYear = matchesData.filter(
      match => match.season === targetYear
    )
    // console.log(matchesInYear);

    const matchIdsInYear = matchesInYear.map(match => match.id)
    // console.log(matchIdsInYear)

    // Filter ball-by-ball data for matches in the specified year
    const relevantBallData = deliveriesData.filter(ball =>
      matchIdsInYear.includes(ball.match_id)
    )
    // console.log(relevantBallData)

    const extraRunsConceded = relevantBallData.reduce((acc, ball) => {
      if (!(ball.bowling_team in acc)) {
        acc[ball.bowling_team] = Number(ball.extra_runs)
      } else {
        acc[ball.bowling_team] += Number(ball.extra_runs)
      }

      return acc
    }, {})
    return extraRunsConceded
  }

  const extraRunsConcededIn2016 = calculateExtraRunsConceded(
    deliveriesData,
    matchesData,
    '2016'
  )

  fs.writeFileSync(
    `./../public/output/extraRunsperTeamIn2016.json`,
    JSON.stringify(extraRunsConcededIn2016, null, 2)
  )
} catch (error) {
  console.log(error)
}
