const matchesData = require('./../public/output/matches.json')
const deliveriesData = require('./../public/output/deliveries.json')
const fs = require('fs')

// 4. Top 10 economical bowlers in the year 2015
try {
  const topEconomicalBowlers = (matchesData, deliveriesData, year, top) => {
    const matchesInYear = matchesData.filter(element => element.season === year)

    const matchIdsInYear = matchesInYear.map(match => match.id)

    const bowlersData = deliveriesData.filter(bowler =>
      matchIdsInYear.includes(bowler.match_id)
    )

    const RunsBalls = bowlersData.reduce((bowlerStats, delivery) => {
      const bowler = delivery.bowler
      const runs = Number(delivery.total_runs) - Number(delivery.bye_runs)
      const balls =
        delivery.noball_runs === '0' && delivery.wide_runs === '0' ? 1 : 0
      if (bowlerStats[bowler]) {
        bowlerStats[bowler].runs += runs
        bowlerStats[bowler].balls += balls
      } else {
        bowlerStats[bowler] = { runs, balls }
      }
      return bowlerStats
    }, {})

    const bowlerEconomy = []

    for (const bowler in RunsBalls) {
      const { runs, balls } = RunsBalls[bowler]
      const economy = (runs / balls) * 6
      bowlerEconomy.push({ bowler, economy })
    }
    bowlerEconomy.sort((a, b) => a.economy - b.economy)

    const EconomicalBowlers = bowlerEconomy.slice(0, top)

    return EconomicalBowlers
  }

  const top10EconomicalBowlers = topEconomicalBowlers(
    matchesData,
    deliveriesData,
    '2015',
    10
  )

  fs.writeFileSync(
    `./../public/output/topEconomicalBowlers.json`,
    JSON.stringify(top10EconomicalBowlers, null, 2)
  )
} catch (error) {
  console.log(error)
}
