const { throws } = require('assert')
const matchesData = require('./../public/output/matches.json')
const fs = require('fs')

// 2.Number of matches won per team per year in IPL.
const result = matchesData => {
  try {
    const matchesWonPerTeamPerYear = matchesData.reduce((acc, match) => {
      const year = match.season
      const winner = match.winner

      if (!acc[year]) {
        acc[year] = {}
      }

      if (!acc[year][winner]) {
        acc[year][winner] = 1
      } else {
        acc[year][winner]++
      }
      return acc
    }, {})
    return matchesWonPerTeamPerYear
  } catch (error) {
    throw new Error(error)
  }
}


try {
  res = result(matchesData)
  fs.writeFileSync(
    `./../public/output/matchesWonPerTeamPerYear.json`,
    JSON.stringify(res, null, 2)
  )
} catch (error) {
  console.log(error)
}
